A=-14.6337;
B=4791.842;
C=-115334;
D=-3730535;

%3000 points in the plot
T = (250:0.05:400)';
[length,width] = size(T);

R = (10e3)*exp(A + (B./T) + (C./(T.^2)) + (D./(T.^3)) );

%if we use a resistor to sense the resistance of the thermistor
ref = [1000,5000,10000,15000,20000,50000];
ref = repmat(ref,length,1);
T = repmat(T,1,6);
R = repmat(R,1,6);

Vo = ((5.*R)./(ref+R))';

%if we quantize to 1204 divisions
Q = floor(Vo*1023./5);

figure(1);
hold on;
grid on;
plot(Q',T-273,'*');
legend('1000','5000','10,000','15,000','20,000','50,000');
print -dpng figure1.png
hold off;

%find the element of the 10K list which has the value 518
ADC = Q(3,:);
tmp = T(:,1)';

I = min(abs(ADC-520))+520;
I = find(ADC == I);
tmp(I)-273
