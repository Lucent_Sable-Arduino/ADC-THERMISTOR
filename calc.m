A = 3.354016e-3;
B = 2.569335e-4;
C = 2.626311e-6;
D = 0.675278e-7;

I = (1:1:1023)';
Vo = (5.*I);
Vo = Vo./1024;

x = 1.1;

R = x.*Vo./(5-Vo);

A= A;
B= B.*reallog(R);
C= C.*realpow(reallog(R), 2);
D= D.*realpow(reallog(R), 3);

T = A+B+C+D;
T = 1./T;
int = fix(T-273.15);
dec = abs(T-int-273.15)*100;

%conver to signed integers
int = int8(int);
dec = int8(dec);
%autogenerate code magic
printf('signed char tempINT[] = {0');
printf(',%.0f ', int);
printf('};\n');

printf('signed char tempDEC[] = {0');
printf(',%.0f ', dec);
printf('};\n');
