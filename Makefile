#variable for clock frequency of the arduino
cf=16000000Ul
#the microcontroller to use
uc=atmega328p
UC=ATMEGA328P
#name of file to compile
file=therm
libs= lcd adc
libpath= ../LIBS .
hpath =  ../LIBS .
#device to upload to
dev=/dev/ttyACM0
#baud-rate to upload at
baud=115200

#Targets that make will accept
all:upload
arr:array

compile:$(file).o

#tasks needed to carry out these targets

###Upload the library demo program
$(file).o:$(file).c
	avr-gcc -Os $(hpath:%=-I%) -DF_CPU=$(cf) -mmcu=$(uc) -c -o $(file).o $(file).c

$(file).raw:$(file).o
	avr-gcc -mmcu=$(uc) $(file).o -o $(file).raw $(libpath:%=-L%) $(libs:%=-l%)

$(file).hex:$(file).raw
	avr-objcopy -O ihex -R .eeprom $(file).raw $(file).hex

upload:$(file).hex
	avrdude -F -c arduino -p $(UC) -P $(dev) -b $(baud) -U flash:w:$(file).hex


array:calc.m
	octave calc.m > array.cpart
	touch $(file).c
