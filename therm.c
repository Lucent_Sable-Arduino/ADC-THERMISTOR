
//included for delay functions
#include <util/delay.h>

#include <avr/io.h>

//included for sprintf
#include <stdio.h>

//included for LCD functions
#include <lcd.h>
#include "adc.h"

//include this file for the precalculated values of temperature
//this file declares a global array "temp[1024]" which can be indexed by
//an adc reading
#include "array.cpart"

int main(void)
{
	char buff[40];
	int val;
	int volt, mil_volt;

	lcd_init();

	adc_init(0);

	while(1)
	{
		val = adc_read();
		sprintf(buff,"Temp:%d.%d C\nADC:%2d", tempINT[val],
			tempDEC[val], val);
		lcd_display(buff);
		_delay_ms(700);
	}

}

